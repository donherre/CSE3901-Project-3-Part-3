Authors: Don Herre, Jessica Kasson, Allen Yu
Description: This document contains information on R.C. McDowell's Page redesign

================

STEP 1 - CHOOSING A PAGE: We chose to redesign Professor McDowell's site because it had sufficient content to meet the requirements of the project. Also it needed work to bring it up to html5 standards. The original website can be found at http://web.cse.ohio-state.edu/~mcdowelr/.

STEP 2 - ASSIGNING WORK TO EACH MEMBER: To complete the redesign certain files were tasked to each member of the team with some crossover when necessary. Below is a list of pertinent information about the project files and work distribution.

	HTML FILES: index.htm, research.htm, teaching.htm, music.htm, family.htm
	CSS FILES: main.css, button.css
	GRAPHIC FILES: Ray.jpg, oval.jpg, footer-coe-logo.png
	
	MEMBER/ASSIGNMENT:

	Don Herre - index.htm, main.css, button.css
	Jessica Kasson - research.htm, teaching.htm
	Allen Yu - music.htm, family.htm

STEP 3 - DESIGN: An initial concept was developed as a template for each page and was modified as necessary to fit specific content. 

STEP 4 - VALIDATION: As per the requirements all html and css code was checked for valid markup. An online validation tool was used and errors were identified and corrected where necessary. The validator can be found at http://validator.w3.org/.

CONCLUSION: The final product of the redesign is a contemporary webpage that is consistent with the content of the original. It includes valid html5 markup that should be renderable by any modern web browser. 

